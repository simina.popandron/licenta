/**
 * @format
 */

import 'react-native-gesture-handler'; //this one has to be the first one
import {AppRegistry} from 'react-native';
import App from './App';
import {name as appName} from './app.json';

AppRegistry.registerComponent(appName, () => App);
