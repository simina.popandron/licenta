/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * Generated with the TypeScript template
 * https://github.com/react-native-community/react-native-template-typescript
 *
 * @format
 */

/*
//initial page
 import React from 'react';
 import {
   SafeAreaView,
   ScrollView,
   StatusBar,
   StyleSheet,
   Text,
   useColorScheme,
   View,
 } from 'react-native';

 import {
   Colors,
   DebugInstructions,
   Header,
   LearnMoreLinks,
   ReloadInstructions,
 } from 'react-native/Libraries/NewAppScreen';

 const Section: React.FC<{
   title: string;
 }> = ({children, title}) => {
   const isDarkMode = useColorScheme() === 'dark';
   return (
     <View style={styles.sectionContainer}>
       <Text
         style={[
           styles.sectionTitle,
           {
             color: isDarkMode ? Colors.white : Colors.black,
           },
         ]}>
         {title}
       </Text>
       <Text
         style={[
           styles.sectionDescription,
           {
             color: isDarkMode ? Colors.light : Colors.dark,
           },
         ]}>
         {children}
       </Text>
     </View>
   );
 };

 const App = () => {
   const isDarkMode = useColorScheme() === 'dark';

   const backgroundStyle = {
     backgroundColor: isDarkMode ? Colors.darker : Colors.lighter,
   };

   return (
     <SafeAreaView style={backgroundStyle}>
       <StatusBar barStyle={isDarkMode ? 'light-content' : 'dark-content'} />
       <ScrollView
         contentInsetAdjustmentBehavior="automatic"
         style={backgroundStyle}>
         <Header />
         <View
           style={{
             backgroundColor: isDarkMode ? Colors.black : Colors.white,
           }}>
           <Section title="HELLO!!!">
             Edit <Text style={styles.highlight}>App.js</Text> to change this
             screen and then come back to see your edits.
           </Section>
           <Section title="hola">
             <ReloadInstructions />
           </Section>
           <Section title="Debug">
             <DebugInstructions />
           </Section>
           <Section title="Learn More">
             Read the docs to discover what to do next:
           </Section>
           <LearnMoreLinks />
         </View>
       </ScrollView>
     </SafeAreaView>
   );
 };

 const styles = StyleSheet.create({
   sectionContainer: {
     marginTop: 32,
     paddingHorizontal: 24,
   },
   sectionTitle: {
     fontSize: 24,
     fontWeight: '600',
   },
   sectionDescription: {
     marginTop: 8,
     fontSize: 18,
     fontWeight: '400',
   },
   highlight: {
     fontWeight: '700',
   },
 });

 export default App;
*/

import React from 'react';
import {
  NavigationContainer,
  NavigationHelpersContext,
} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
import {createDrawerNavigator} from '@react-navigation/drawer';
import Icon from 'react-native-vector-icons/Ionicons';
import {createMaterialBottomTabNavigator} from '@react-navigation/material-bottom-tabs';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';

import MainTabScreen from './src/screens/MainTabScreen';
import {DrawerContent} from './src/screens/DrawerContent';
import SettingsScreen from './src/screens/SettingsScreen';
import {View, Text} from 'react-native';
import Map from './src/Map';
import BackgroundDemo from './src/BackgroundDemo';
import CurrentLocation from './src/CurrentLocation';
import HomeScreen from './src/screens/HomeScreen';
import DetailsScreen from './src/screens/DetailsScreen';
import {Button} from 'react-native-paper';
import ChooseDestination from './src/ChooseDestination';
import ProfileScreen from './src/screens/ProfileScreen';
import {useState} from 'react';
import {createContext} from 'react';
import MainTabScreenAdmin from './src/screens/MainTabScreenAdmin';
import RootStackScreen from './src/RootStackScreen';
import LogoutButton from './src/screens/LogoutButton';
import ScanQR from './src/screens/ScanQR';

interface ILocation {
  latitude: number;
  longitude: number;
}

// interface IUser {
//   role: String;
//   id: number;
//   username: String;
// }

interface IUser {
  token: string;
  id: number | null;
  permission: string | null;
  username: string | null;
  photo: string | null;
}

interface IRoute {
  transportLine: number;
}

const HomeStack = createStackNavigator();
const DetailsStack = createStackNavigator();

const Drawer = createDrawerNavigator();

// const DetailsStackScreen = ({navigation}: any) => (
//   <DetailsStack.Navigator
//     screenOptions={{
//       headerStyle: {
//         backgroundColor: '#1f65ff',
//       },
//       headerTintColor: '#fff',
//       headerTitleStyle: {
//         fontWeight: 'bold',
//       },
//     }}>
//     <DetailsStack.Screen
//       name="Details"
//       component={DetailsScreen}
//       options={{
//         headerLeft: () => (
//           <Icon.Button
//             name="menu"
//             size={25}
//             backgroundColor="#1f65ff"
//             onPress={() => navigation.openDrawer()}></Icon.Button>
//         ),
//       }}
//     />
//   </DetailsStack.Navigator>
// );

export const DestinationContext = createContext({
  contextDestination: {} as ILocation,
  setContextDestination: (destination: ILocation) => {},
});

export const UserContext = createContext({
  contextUser: {} as IUser,
  setContextUser: (user: IUser) => {},
});

export const RouteContext = createContext({
  contextRoute: {} as IRoute,
  setContextRoute: (route: IRoute) => {},
});
const App = () => {
  const [hello, setHello] = useState('hello there');
  const [contextDestination, setContextDestination] = useState<ILocation>();
  const [contextUser, setContextUser] = useState<IUser>();
  const [contextRoute, setContextRoute] = useState<IRoute>();

  const customSetDestination = (destination: ILocation) => {
    //console.log('here ', destination);
    setContextDestination(destination);
  };

  const customSetUser = (user: IUser) => {
    console.log('the user is setting: ', user);
    setContextUser(user);
  };
  const customSetRoute = (route: IRoute) => {
    console.log('the route is setting: ', route);
    setContextRoute(route);
  };

  const value = {
    contextDestination: contextDestination as ILocation,
    setContextDestination: customSetDestination,
  };

  const valueUser = {
    contextUser: contextUser as IUser,
    setContextUser: customSetUser,
  };

  const valueRoute = {
    contextRoute: contextRoute as IRoute,
    setContextRoute: customSetRoute,
  };
  return (
    <RouteContext.Provider value={valueRoute}>
      <UserContext.Provider value={valueUser}>
        <DestinationContext.Provider value={value}>
          {/* {contextUser?.username ? (
          <NavigationContainer>
            <Drawer.Navigator initialRouteName="Home">
              <Drawer.Screen name="Home">
                {_ => <MainTabScreen helloText={hello} />}
              </Drawer.Screen>
              <Drawer.Screen name="Top" component={DetailsStackScreen} />
              <Drawer.Screen
                name="ChooseDestination"
                component={ChooseDestination}
              />
              <Drawer.Screen name="ProfileScreen" component={ProfileScreen} />
            </Drawer.Navigator>
          </NavigationContainer>
        ) : (
          <NavigationContainer>
            <Drawer.Navigator initialRouteName="Home">
              <Drawer.Screen name="Home" component={HomeScreen} />
            </Drawer.Navigator>
          </NavigationContainer>
        )} */}

          {/* <NavigationContainer>
          <RootStackScreen />
        </NavigationContainer> */}

          {(() => {
            //login
            if (contextUser?.username == null) {
              return (
                <NavigationContainer>
                  <RootStackScreen />
                </NavigationContainer>
              );
            } else {
              //user
              if (contextUser?.permission == '2') {
                return (
                  <NavigationContainer>
                    <Drawer.Navigator initialRouteName="Home">
                      <Drawer.Screen name="Home">
                        {_ => <MainTabScreen helloText={hello} />}
                      </Drawer.Screen>
                    </Drawer.Navigator>
                  </NavigationContainer>
                );
              } else {
                //admin
                if (contextUser.permission == '1') {
                  return (
                    // <View>
                    //   <Text>Esti admin bravo</Text>
                    // </View>
                    <NavigationContainer>
                      <Drawer.Navigator initialRouteName="Home">
                        <Drawer.Screen name="Home">
                          {_ => <MainTabScreenAdmin helloText={hello} />}
                        </Drawer.Screen>
                      </Drawer.Navigator>
                    </NavigationContainer>
                  );
                }
              }
            }

            return null;
          })()}
        </DestinationContext.Provider>
      </UserContext.Provider>
    </RouteContext.Provider>
  );
};

export default App;
