import React from 'react';

import {
  View,
  Text,
  StyleSheet,
  Dimensions,
  Button,
  Image,
  TouchableOpacity,
  Alert,
  Platform,
  TextInput,
  StatusBar,
} from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import Feather from 'react-native-vector-icons/Feather';
import styled from 'styled-components';
import {Line} from 'react-native-svg';
import RegisterScreen from './RegisterScreen';
import {UserContext} from '../App';
import axios from 'axios';
import jwt_decode from 'jwt-decode';
import {isDecimal} from 'geolib';

const API = 'http://10.0.2.2:4020';

interface IUser {
  permission: string;
  id: number;
  username: string;
  photo: string;
}

interface ILoginStateData {
  username: string;
  password: string;
  check_textInputChange: boolean;
  secureTextEntry: boolean;
  tokenState: string;
  decodedState: IUser;
}

// const loginStateInitialData :ILoginStateData ={
//     username: '',
//     password: '',
//     check_textInputChange: false,
//     secureTextEntry: true,
//     tokenState: '',
//     decodedState:{
//         permission: '1',
//         id: 0,
//         username: '',
//         photo: ''
//     }
// }

interface LoggedUserInfo {
  token: string;
  decodedState: IUser | null;
}

const LoginScreen = ({navigation}: any) => {
  const [data, setData] = React.useState({
    username: '',
    password: '',
    check_textInputChange: false,
    secureTextEntry: true,
  });

  const [loggedUser, setLoggedUser] = React.useState<LoggedUserInfo>({
    token: '',
    decodedState: null,
  });

  const textInputChange = (val: any) => {
    if (val.length != 0) {
      setData({
        ...data,
        username: val,
        check_textInputChange: true,
      });
    } else {
      setData({
        ...data,
        username: val,
        check_textInputChange: false,
      });
    }
  };
  const handlePasswordChange = (val: any) => {
    setData({
      ...data,
      password: val,
    });
  };
  const updateSecureTextEntry = () => {
    setData({
      ...data,
      secureTextEntry: !data.secureTextEntry,
    });
  };

  const loginCall = async (user: string, pass: string) => {
    console.log('BEFORE AWAIT');

    axios
      .post(API + '/auth/login', {
        username: user,
        password: pass,
      })
      .then(response => {
        //console.log('API Response', response.data);
        const token = response.data['access_token'];
        const decoded: IUser = jwt_decode(token);
        console.log('decoded: ', decoded);
        console.log('------------------------');
        // routeCall(decoded.id, token);

        setLoggedUser({
          token: token,
          decodedState: decoded,
        });

        console.log('Logged User: ', loggedUser);
        //console.log('!!!UseContext:', useContext(UserContext));

        // setContextUser({
        //   token: loggedUser.token,
        //   id: decoded.id,
        //   permission: decoded.permission,
        //   username: decoded.username,
        //   photo: decoded.photo,
        // });
      })
      .catch(err => console.log(err));
  };

  const routeCall = async (userId: number, token: string) => {
    axios
      .get(API + `/routes/${userId}`, {
        headers: {Authorization: `Bearer ${token}`},
      })
      .then(response => console.log('ROUTES: ', response))
      .catch(err => console.log(err));
  };

  return (
    <UserContext.Consumer>
      {({contextUser, setContextUser}) => (
        <View style={styles.container}>
          <StatusBar backgroundColor="#009387" barStyle="light-content" />
          <View style={styles.header}>
            <Text style={styles.text_header}>Hello!</Text>
          </View>
          <View style={styles.footer}>
            <Text style={styles.text_footer}>Username</Text>
            <View style={styles.action}>
              <FontAwesome name="user-o" color="black" size={20} />
              <TextInput
                placeholder="Your username"
                style={styles.textInput}
                autoCapitalize="none"
                onChangeText={val => textInputChange(val)}
              />
              {data.check_textInputChange ? (
                <Feather name="check-circle" color="green" size={20} />
              ) : null}
            </View>
            <Text style={[styles.text_footer, {marginTop: 35}]}>Password</Text>
            <View style={styles.action}>
              <FontAwesome name="lock" color="black" size={20} />
              <TextInput
                placeholder="Your password"
                secureTextEntry={data.secureTextEntry ? true : false}
                style={styles.textInput}
                autoCapitalize="none"
                onChangeText={val => handlePasswordChange(val)}
              />
              <TouchableOpacity onPress={updateSecureTextEntry}>
                {data.secureTextEntry ? (
                  <Feather name="eye-off" color="grey" size={20} />
                ) : (
                  <Feather name="eye" color="grey" size={20} />
                )}
              </TouchableOpacity>
            </View>
            <View style={styles.button}>
              <TouchableOpacity
                onPress={() => {
                  loginCall(data.username, data.password);
                  setContextUser({
                    token: loggedUser.token,
                    id: loggedUser.decodedState?.id ?? null,
                    permission: loggedUser.decodedState?.permission ?? null,
                    username: loggedUser.decodedState?.username ?? null,
                    photo: loggedUser.decodedState?.photo ?? null,
                  });
                  console.log('Data din login', data);
                  console.log('context user din login', contextUser);
                }}
                // onPress={() => {
                //   if (data.username == 'admin') {
                //     setContextUser({
                //       role: 'admin',
                //       id: 1,
                //       username: data.username,
                //     });
                //   } else {
                //     setContextUser({
                //       role: 'user',
                //       id: 2,
                //       username: data.username,
                //     });
                //   }
                //   //   console.log('username:' + contextUser.username);
                //   //   console.log('password:' + data.password);
                //   //   console.log('id:' + contextUser.id);
                //   //   console.log('role:' + contextUser.role);
                // }}
                style={[
                  styles.signIn,
                  {
                    borderColor: '#009387',
                    borderWidth: 1,
                    marginTop: 15,
                  },
                ]}>
                <LinearGradient
                  colors={['#08d4c4', '#01ab9d']}
                  style={styles.signIn}>
                  <Text style={[styles.textSign, {color: '#fff'}]}>Login</Text>
                </LinearGradient>
              </TouchableOpacity>
            </View>
            <TouchableOpacity
              onPress={() => navigation.navigate('RegisterScreen')}
              style={[
                styles.signIn,
                {
                  borderColor: '#009387',
                  borderWidth: 1,
                  marginTop: 15,
                },
              ]}>
              <Text style={[styles.textSign, {color: '#009387'}]}>
                Register
              </Text>
            </TouchableOpacity>
          </View>
        </View>
      )}
    </UserContext.Consumer>
  );
};

export default LoginScreen;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#009387',
  },
  header: {
    flex: 1,
    justifyContent: 'flex-end',
    paddingHorizontal: 20,
    paddingBottom: 50,
  },
  footer: {
    flex: 3,
    backgroundColor: '#fff',
    borderTopLeftRadius: 30,
    borderTopRightRadius: 30,
    paddingHorizontal: 20,
    paddingVertical: 30,
  },
  text_header: {
    color: '#fff',
    fontWeight: 'bold',
    fontSize: 30,
  },
  text_footer: {
    color: '#05375a',
    fontSize: 18,
  },
  action: {
    flexDirection: 'row',
    marginTop: 10,
    borderBottomWidth: 1,
    borderBottomColor: '#f2f2f2',
    paddingBottom: 5,
  },
  actionError: {
    flexDirection: 'row',
    marginTop: 10,
    borderBottomWidth: 1,
    borderBottomColor: '#FF0000',
    paddingBottom: 5,
  },
  textInput: {
    flex: 1,
    marginTop: Platform.OS === 'ios' ? 0 : -12,
    paddingLeft: 10,
    color: '#05375a',
  },
  errorMsg: {
    color: '#FF0000',
    fontSize: 14,
  },
  button: {
    alignItems: 'center',
    marginTop: 50,
  },
  signIn: {
    width: '100%',
    height: 50,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 10,
  },
  textSign: {
    fontSize: 18,
    fontWeight: 'bold',
  },
});
