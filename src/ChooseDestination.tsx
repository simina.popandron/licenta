import React, {Component} from 'react';
import {
  View,
  Text,
  StyleSheet,
  TouchableOpacity,
  Image,
  Modal,
  Alert,
  ScrollView,
} from 'react-native';
import {useState, useEffect} from 'react';
import Geolocation from 'react-native-geolocation-service';
import MapView, {
  Callout,
  Marker,
  PROVIDER_GOOGLE,
  Polyline,
} from 'react-native-maps';
import MapViewDirections from 'react-native-maps-directions';
import {Provider, Toast, Button, Popover, Icon} from '@ant-design/react-native';
import Geocoder from 'react-native-geocoding';
import {GooglePlacesAutocomplete} from 'react-native-google-places-autocomplete';

const GOOGLE_APIKEY = 'AIzaSyDovK_UBLIkoSSssODQWttNAMpVvu_3lV8';
Geocoder.init(GOOGLE_APIKEY); // use a valid API key
interface ILocation {
  lat: number;
  lng: number;
}

const GooglePlacesInput = ({navigation}: any) => {
  const [destination, setDestination] = useState<ILocation | undefined>();
  const [destinationText, setDestinationText] = useState<String>(
    'NO DESTINATION CHOSEN YET',
  );

  return (
    <ScrollView keyboardShouldPersistTaps={'handled'}>
      <GooglePlacesAutocomplete
        placeholder="Search"
        onPress={(data, details = null) => {
          // 'details' is provided when fetchDetails = true
          console.log(data.description);
          setDestinationText(data.description);
          //console.log(" ******* DESTINATION TEXT ");
          //console.log(destinationText);
          //console.log( details);

          Geocoder.from(data.description)
            .then(json => {
              var location = json.results[0].geometry.location;
              console.log('NOW YOU HAVE LAT&LONG FOR THE DESTINATION');
              console.log(location);
              setDestination(location);
            })
            .catch(error => console.warn(error));
        }}
        query={{
          key: GOOGLE_APIKEY,
          language: 'en',
          components: 'country:ro',
        }}
      />
      <Text>DESTINATION: {destinationText}</Text>

      <TouchableOpacity
        style={styles.button}
        onPress={() => {
          navigation.navigate('Map');
        }}>
        <Text style={styles.buttonText}>Finish</Text>
      </TouchableOpacity>
    </ScrollView>
  );
};

export default GooglePlacesInput;

const styles = StyleSheet.create({
  roundButton1: {
    width: 75,
    height: 75,
    justifyContent: 'center',
    alignItems: 'center',
    padding: 10,
    borderRadius: 100,
    backgroundColor: 'red',
  },
  buttonText: {
    color: '#FFFFFF',
    fontSize: 22,
  },
  container: {
    ...StyleSheet.absoluteFillObject,
    height: '100%',
    width: '100%',
    justifyContent: 'flex-end',
    alignItems: 'center',
  },
  footer: {
    height: 50,
    display: 'flex',
    borderRadius: 6,
    justifyContent: 'center',
    alignItems: 'center',
    width: '50%',
    backgroundColor: '#2AC062',
    shadowColor: '#2AC062',
    shadowOpacity: 0.5,
    shadowOffset: {
      height: 10,
      width: 0,
    },
    position: 'absolute',
    top: 600,
    left: 90,
  },
  button: {
    display: 'flex',
    height: 50,
    borderRadius: 6,
    justifyContent: 'center',
    alignItems: 'center',
    width: '50%',
    backgroundColor: '#2AC062',
    shadowColor: '#2AC062',
    shadowOpacity: 0.5,
    shadowOffset: {
      height: 10,
      width: 0,
    },
    shadowRadius: 25,
  },
});
