import axios from 'axios';
import {getPreciseDistance} from 'geolib';
import React, {useContext, useEffect, useState} from 'react';
import {
  Alert,
  Modal,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import Geolocation from 'react-native-geolocation-service';
import MapView, {
  Callout,
  Marker,
  Polyline,
  PROVIDER_GOOGLE,
} from 'react-native-maps';
import MapViewDirections from 'react-native-maps-directions';
import {DestinationContext, RouteContext, UserContext} from '../App';
import DestinationModal from './DestinationModal';
import Icon from 'react-native-vector-icons/Ionicons';

interface ILocation {
  latitude: number;
  longitude: number;
}

interface IRoute {
  path: ILocation[];
  transportType: number;
  transportLine: number;
  distance: number;
  steps: number;
}

const origin = {latitude: 46.77772, longitude: 23.59351};
const destination = {latitude: 46.7789897, longitude: 23.6164478};
const GOOGLE_MAPS_APIKEY = 'AIzaSyDovK_UBLIkoSSssODQWttNAMpVvu_3lV8';

const API = 'http://10.0.2.2:4020';

const CurrentLocation = ({helloText, navigation}: any) => {
  const [isVisible, setIsVisible] = useState<boolean>(false);
  const [routeDistance, setRouteDistance] = useState<number>(0);
  const [location, setLocation] = useState<ILocation | undefined>(origin);
  const [startLocation, setStartLocation] = useState<ILocation | undefined>();
  const [ongoingRoute, setOngoingRoute] = useState<boolean>(false);
  const [destination, setDestination] = useState<ILocation | undefined>(origin);
  const [routeCoord, setRouteCoord] = useState<ILocation[]>([]);
  const userInfo = useContext(UserContext);

  const [newCoord, setNewCoord] =
    useState<ILocation | undefined>(startLocation);

  const [contextProvidedDestination, setContextProvidedDestination] =
    useState('');

  const displayModal = (show: boolean) => {
    setIsVisible(show);
  };

  const buttonClickedHandler = () => {
    console.log('You have been clicked a button!');
  };

  useEffect(() => {
    const _watchId = Geolocation.watchPosition(
      (position: {coords: {latitude: any; longitude: any}}) => {
        const {latitude, longitude} = position.coords;
        setLocation({latitude, longitude});
        //console.log('new location = ', {latitude, longitude});
      },
      (error: any) => {
        console.log(error);
      },
      {
        enableHighAccuracy: true,
        distanceFilter: 0,
        interval: 10000,
        fastestInterval: 10000,
      },
    );

    return () => {
      if (_watchId) {
        Geolocation.clearWatch(_watchId);
      }
    };
  }, []);

  useEffect(() => {
    const newRouteCoords: ILocation[] =
      location && ongoingRoute ? [...routeCoord, location] : [];
    //console.log('new route coords', newRouteCoords);

    setRouteCoord(newRouteCoords);
    console.log('ROUTE ARRAY = ', routeCoord);
  }, [location]);

  const calculatePreciseDistance = (start: ILocation, last: ILocation) => {
    var pdis = getPreciseDistance(
      {latitude: start.latitude, longitude: start.longitude},
      {latitude: last.latitude, longitude: last.longitude},
    );
    setRouteDistance(pdis);
    return console.log(
      'Precise Distance ',
      pdis,
      ' Meter OR ',
      pdis / 1000,
      ' KM',
    );
  };

  //values for transporType
  //     bus = 1,
  //     car=2,
  //     walk=3,
  //     bike=4,
  const createRouteCall = (
    token: string,
    path: ILocation[],
    transportType: number,
    transportLine: number,
    distance: number,
    steps: number,
  ) => {
    //console.log('!!!!!!!!!!!!!!in route call');
    const route: IRoute = {path, transportType, transportLine, distance, steps};
    axios
      .post(API + '/route-create', route, {
        headers: {Authorization: `Bearer ${token}`},
      })
      .then(response => {
        console.log('route created:', response.data);
      })
      .catch(err => console.log(err));
    return;
  };

  return (
    <RouteContext.Consumer>
      {({contextRoute, setContextRoute}) => {
        return (
          <DestinationContext.Consumer>
            {({contextDestination, setContextDestination}) => {
              //console.log('received context destination', contextDestination);

              return (
                <View style={styles.container}>
                  {location ? (
                    <View style={styles.container}>
                      <MapView
                        provider={PROVIDER_GOOGLE} // remove if not using Google Maps
                        style={styles.map}
                        region={{
                          latitude: location.latitude,
                          longitude: location.longitude,
                          latitudeDelta: 0.0155,
                          longitudeDelta: 0.0122,
                        }}>
                        <View>
                          {contextDestination ? (
                            <Marker
                              coordinate={{
                                longitude: contextDestination?.longitude,
                                latitude: contextDestination?.latitude,
                              }}>
                              <Callout>
                                <View>
                                  <Text>DESTINATION</Text>
                                </View>
                              </Callout>
                            </Marker>
                          ) : null}
                          <Marker
                            coordinate={{
                              longitude: location.longitude,
                              latitude: location.latitude,
                            }}>
                            <Callout>
                              <View>
                                <Text>CURRENT POSITION</Text>
                              </View>
                            </Callout>
                          </Marker>
                          <MapViewDirections
                            origin={{
                              longitude: contextDestination?.longitude,
                              latitude: contextDestination?.latitude,
                            }}
                            destination={startLocation} //start(where the marker is)
                            apikey={GOOGLE_MAPS_APIKEY}
                            strokeWidth={6}
                            strokeColor="#70e000" //green
                            mode="WALKING"
                          />

                          <MapViewDirections
                            origin={{
                              longitude: contextDestination?.longitude,
                              latitude: contextDestination?.latitude,
                            }}
                            destination={startLocation} //start(where the marker is)
                            apikey={GOOGLE_MAPS_APIKEY}
                            strokeWidth={4}
                            strokeColor="red" //orange
                            mode="TRANSIT"
                          />

                          <MapViewDirections
                            origin={{
                              longitude: contextDestination?.longitude,
                              latitude: contextDestination?.latitude,
                            }}
                            destination={startLocation} //start(where the marker is)
                            apikey={GOOGLE_MAPS_APIKEY}
                            strokeWidth={2}
                            strokeColor="black"
                            mode="DRIVING"
                          />
                          {ongoingRoute
                            ? (console.log('*este ruta*'),
                              console.log(
                                'CONTEXT DESTINATION IN CURRENTLOCATION',
                                contextDestination?.latitude,
                              ),
                              calculatePreciseDistance(
                                startLocation as ILocation,
                                location,
                              ),
                              (
                                <Polyline
                                  coordinates={routeCoord}
                                  strokeWidth={9}
                                  strokeColor="#c879ff" //purple
                                />
                              ))
                            : //console.log('nu este ruta')
                              console.log('')}
                        </View>
                      </MapView>
                      <View style={styles.containermodal}>
                        <Modal
                          animationType={'slide'}
                          transparent={false}
                          visible={isVisible}
                          onRequestClose={() => {
                            Alert.alert('Modal has now been closed.');
                          }}>
                          <DestinationModal />

                          <TouchableOpacity
                            style={styles.footer}
                            onPress={() => {
                              displayModal(!isVisible);
                              setStartLocation(location);
                              setOngoingRoute(true);
                            }}>
                            <Text style={styles.buttonText}>Finish</Text>
                          </TouchableOpacity>
                        </Modal>
                      </View>

                      {ongoingRoute ? (
                        <TouchableOpacity
                          style={styles.roundButton1}
                          onPress={() => {
                            if (!contextRoute?.transportLine) {
                              createRouteCall(
                                userInfo.contextUser.token,
                                routeCoord,
                                4, //altceva
                                0,
                                routeDistance,
                                0,
                              );
                            } else {
                              createRouteCall(
                                userInfo.contextUser.token,
                                routeCoord,
                                1, //bus
                                contextRoute.transportLine,
                                routeDistance,
                                0,
                              );
                            }
                            setOngoingRoute(false);
                            setContextRoute({transportLine: 0});
                          }}>
                          <Text style={styles.buttonText}>Stop</Text>
                        </TouchableOpacity>
                      ) : (
                        <TouchableOpacity
                          style={styles.roundButton1}
                          onPress={() => {
                            displayModal(true);
                          }}>
                          <Text style={styles.buttonText}>Start</Text>
                        </TouchableOpacity>
                      )}
                    </View>
                  ) : (
                    <Text>Loading Map...</Text>
                  )}
                </View>
              );
            }}
          </DestinationContext.Consumer>
        );
      }}
    </RouteContext.Consumer>
  );
};

export default CurrentLocation;

const styles = StyleSheet.create({
  container: {
    ...StyleSheet.absoluteFillObject,
    height: '100%',
    width: '100%',
    justifyContent: 'flex-end',
    alignItems: 'center',
  },
  map: {
    ...StyleSheet.absoluteFillObject,
  },
  containermodal: {
    padding: 25,
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  button: {
    display: 'flex',
    height: 50,
    borderRadius: 6,
    justifyContent: 'center',
    alignItems: 'center',
    width: '50%',
    backgroundColor: '#2AC062',
    shadowColor: '#2AC062',
    shadowOpacity: 0.5,
    shadowOffset: {
      height: 10,
      width: 0,
    },
    shadowRadius: 25,
  },
  closeButton: {
    display: 'flex',
    height: 60,
    borderRadius: 6,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#FF3974',
    shadowColor: '#2AC062',
    shadowOpacity: 0.5,
    shadowOffset: {
      height: 10,
      width: 0,
    },
    shadowRadius: 25,
  },
  buttonText: {
    color: '#FFFFFF',
    fontSize: 22,
  },
  image: {
    marginTop: 150,
    marginBottom: 10,
    width: '100%',
    height: 350,
  },
  text: {
    fontSize: 24,
    marginBottom: 30,
    padding: 40,
  },
  closeText: {
    fontSize: 24,
    color: '#00479e',
    textAlign: 'center',
  },
  roundButton1: {
    width: 75,
    height: 75,
    justifyContent: 'center',
    alignItems: 'center',
    padding: 10,
    borderRadius: 100,
    backgroundColor: 'red',
  },
  footer: {
    height: 50,
    display: 'flex',
    borderRadius: 6,
    justifyContent: 'center',
    alignItems: 'center',
    width: '50%',
    backgroundColor: '#2AC062',
    shadowColor: '#2AC062',
    shadowOpacity: 0.5,
    shadowOffset: {
      height: 10,
      width: 0,
    },
    position: 'absolute',
    top: 600,
    left: 90,
  },
});
