// import React from 'react';
// import { useState } from 'react';
// import {StyleSheet, Text, TouchableOpacity, View} from 'react-native';
// import MapView, {Callout, Marker, PROVIDER_GOOGLE} from 'react-native-maps';
// import { GooglePlacesAutocomplete } from 'react-native-google-places-autocomplete';

// const GOOGLE_APIKEY = 'AIzaSyDovK_UBLIkoSSssODQWttNAMpVvu_3lV8';

//   const DestinationModal = ({navigation}:any) => {

//     const [isVisible, setIsVisible] = useState<boolean>(false);

//     const displayModal = (show: boolean) => {
//         setIsVisible(show)
//       };

//     return (
//         <View>
//                 <GooglePlacesAutocomplete
//                     placeholder='Search'
//                     onPress={(data, details = null) => {
//                         // 'details' is provided when fetchDetails = true
//                         console.log(data, details);
//                     }}
//                     query={{
//                         key: GOOGLE_APIKEY,
//                         language: 'en',
//                         components: 'country:ro',
//                     }}
//                 />

//         </View>
//     );
//   };

//   export default DestinationModal;

//   const styles = StyleSheet.create({

//     roundButton1: {
//         width: 75,
//         height: 75,
//         justifyContent: 'center',
//         alignItems: 'center',
//         padding: 10,
//         borderRadius: 100,
//         backgroundColor: 'red',
//       },
//     buttonText: {
//         color: '#FFFFFF',
//         fontSize: 22,
//     },
//   });

import React, {useState} from 'react';
import {ScrollView, StyleSheet, Text} from 'react-native';
import Geocoder from 'react-native-geocoding';
import {GooglePlacesAutocomplete} from 'react-native-google-places-autocomplete';
import {DestinationContext} from '../App';

const GOOGLE_APIKEY = 'AIzaSyDovK_UBLIkoSSssODQWttNAMpVvu_3lV8';

Geocoder.init(GOOGLE_APIKEY); // use a valid API key

interface ILocation {
  lat: number;
  lng: number;
}

// interface ILocation {
//     latitude: number;
//     longitude: number;
//   }

const DestinationModal = ({navigation}: any) => {
  const [destination, setDestination] = useState<ILocation | undefined>();
  //const [destinationfinal, setDestinationFinal] = useState<ILocation | undefined>();
  const [destinationText, setDestinationText] = useState<String>(
    'NO DESTINATION CHOSEN YET',
  );
  const [lati, setLati] = useState<number>();
  const [longi, setLongi] = useState<number>();
  return (
    <DestinationContext.Consumer>
      {({contextDestination, setContextDestination}) => (
        <ScrollView keyboardShouldPersistTaps={'handled'}>
          <GooglePlacesAutocomplete
            placeholder="Search"
            onPress={(data, details = null) => {
              // 'details' is provided when fetchDetails = true
              console.log(data.description);
              setDestinationText(data.description);
              //console.log(" ******* DESTINATION TEXT ");
              //console.log(destinationText);
              //console.log( details);

              Geocoder.from(data.description)
                .then(json => {
                  const location = json.results[0].geometry.location;
                  console.log('NOW YOU HAVE LAT&LONG FOR THE DESTINATION');
                  console.log(location);
                  //setDestinationFinal()
                  setLati(location.lat);
                  setLongi(location.lng);
                  //setDestinationFinal({lati,logi});
                  setContextDestination({
                    latitude: location.lat,
                    longitude: location.lng,
                  });
                  setDestination(location);
                })
                .catch(error => console.warn(error));
            }}
            query={{
              key: GOOGLE_APIKEY,
              language: 'en',
              components: 'country:ro',
            }}
          />
          <Text>DESTINATION: {destinationText}</Text>
          {/* <TouchableOpacity
                        style={styles.button}
                        onPress={() => {
                            navigation.navigate('Map');
                        }}>
                        <Text style={styles.buttonText}>Finish</Text>
                    </TouchableOpacity> */}
        </ScrollView>
      )}
    </DestinationContext.Consumer>
  );
};

export default DestinationModal;

const styles = StyleSheet.create({
  roundButton1: {
    width: 75,
    height: 75,
    justifyContent: 'center',
    alignItems: 'center',
    padding: 10,
    borderRadius: 100,
    backgroundColor: 'red',
  },
  buttonText: {
    color: '#FFFFFF',
    fontSize: 22,
  },
  container: {
    ...StyleSheet.absoluteFillObject,
    height: '100%',
    width: '100%',
    justifyContent: 'flex-end',
    alignItems: 'center',
  },
  footer: {
    height: 50,
    display: 'flex',
    borderRadius: 6,
    justifyContent: 'center',
    alignItems: 'center',
    width: '50%',
    backgroundColor: '#2AC062',
    shadowColor: '#2AC062',
    shadowOpacity: 0.5,
    shadowOffset: {
      height: 10,
      width: 0,
    },
    position: 'absolute',
    top: 600,
    left: 90,
  },
  button: {
    display: 'flex',
    height: 50,
    borderRadius: 6,
    justifyContent: 'center',
    alignItems: 'center',
    width: '50%',
    backgroundColor: '#2AC062',
    shadowColor: '#2AC062',
    shadowOpacity: 0.5,
    shadowOffset: {
      height: 10,
      width: 0,
    },
    shadowRadius: 25,
  },
});
