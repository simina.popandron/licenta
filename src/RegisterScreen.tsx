import axios from 'axios';
import React from 'react';
import {
  Platform,
  StatusBar,
  StyleSheet,
  Text,
  TextInput,
  TouchableOpacity,
  View,
} from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import Feather from 'react-native-vector-icons/Feather';
import FontAwesome from 'react-native-vector-icons/FontAwesome';

const API = 'http://10.0.2.2:4020';

interface IRegisterUser {
  username: string;
  password: string;
  photo: string;
  dateOfBirth: string;
}
const LoginScreen = ({navigation}: any) => {
  const [data, setData] = React.useState({
    username: '',
    password: '',
    dateOfBirth: '',
    photo: '',
    check_textInputChange: false,
    check_dateInputChange: false,
    check_photoInputChange: false,
    secureTextEntry: true,
    confirm_secureTextEntry: true,
    confirm_password: true,
  });

  const textInputChange = (val: any) => {
    if (val.length != 0) {
      setData({
        ...data,
        username: val,
        check_textInputChange: true,
      });
    } else {
      setData({
        ...data,
        username: val,
        check_textInputChange: false,
      });
    }
  };

  const dateInputChange = (val: any) => {
    if (val.length != 0) {
      setData({
        ...data,
        dateOfBirth: val,
        check_dateInputChange: true,
      });
    } else {
      setData({
        ...data,
        dateOfBirth: val,
        check_dateInputChange: false,
      });
    }
  };
  const photoInputChange = (val: any) => {
    if (val.length != 0) {
      setData({
        ...data,
        photo: val,
        check_photoInputChange: true,
      });
    } else {
      setData({
        ...data,
        photo: val,
        check_photoInputChange: false,
      });
    }
  };

  const handlePasswordChange = (val: any) => {
    setData({
      ...data,
      password: val,
    });
  };
  const handleConfirmPasswordChange = (val: any) => {
    setData({
      ...data,
      confirm_password: val,
    });
  };
  const updateSecureTextEntry = () => {
    setData({
      ...data,
      secureTextEntry: !data.secureTextEntry,
    });
  };
  const updateConfirmSecureTextEntry = () => {
    setData({
      ...data,
      confirm_secureTextEntry: !data.confirm_secureTextEntry,
    });
  };

  const registerCall = (
    username: string,
    password: string,
    dateOfBirth: string,
    photo: string,
  ) => {
    const user: IRegisterUser = {
      username,
      password,
      dateOfBirth,
      photo,
    };

    axios
      .post(API + '/user-create', user)
      .then(response => {
        console.log(response.data);
      })
      .catch(err => console.log(err));
    return;
  };

  return (
    <View style={styles.container}>
      <StatusBar backgroundColor="#009387" barStyle="light-content" />
      <View style={styles.header}>
        <Text style={styles.text_header}>Register now!</Text>
      </View>
      <View style={styles.footer}>
        <Text style={styles.text_footer}>Username</Text>
        <View style={styles.action}>
          <FontAwesome name="user-o" color="black" size={20} />
          <TextInput
            placeholder="Your username"
            style={styles.textInput}
            autoCapitalize="none"
            onChangeText={val => textInputChange(val)}
          />
          {data.check_textInputChange ? (
            <Feather name="check-circle" color="green" size={20} />
          ) : null}
        </View>

        <Text style={[styles.text_footer, {marginTop: 35}]}>Password</Text>
        <View style={styles.action}>
          <FontAwesome name="lock" color="black" size={20} />
          <TextInput
            placeholder="Your password"
            secureTextEntry={data.secureTextEntry ? true : false}
            style={styles.textInput}
            autoCapitalize="none"
            onChangeText={val => handlePasswordChange(val)}
          />
          <TouchableOpacity onPress={updateSecureTextEntry}>
            {data.secureTextEntry ? (
              <Feather name="eye-off" color="grey" size={20} />
            ) : (
              <Feather name="eye" color="grey" size={20} />
            )}
          </TouchableOpacity>
        </View>

        <Text style={styles.text_footer}>Date of birth</Text>
        <View style={styles.action}>
          <FontAwesome name="calendar" color="black" size={20} />
          <TextInput
            placeholder="yyyy-mm-dd"
            style={styles.textInput}
            autoCapitalize="none"
            onChangeText={val => dateInputChange(val)}
          />
          {data.check_dateInputChange ? (
            <Feather name="check-circle" color="green" size={20} />
          ) : null}
        </View>

        <Text style={styles.text_footer}>Photo</Text>
        <View style={styles.action}>
          <FontAwesome name="camera" color="black" size={20} />
          <TextInput
            placeholder="Photo link"
            style={styles.textInput}
            autoCapitalize="none"
            onChangeText={val => photoInputChange(val)}
          />
          {data.check_photoInputChange ? (
            <Feather name="check-circle" color="green" size={20} />
          ) : null}
        </View>

        <View style={styles.button}>
          <LinearGradient colors={['#08d4c4', '#01ab9d']} style={styles.signIn}>
            <TouchableOpacity
              onPress={() => {
                registerCall(
                  data.username,
                  data.password,
                  data.dateOfBirth,
                  data.photo,
                );
              }}>
              <Text style={[styles.textSign, {color: '#fff'}]}>Sign up</Text>
            </TouchableOpacity>
          </LinearGradient>
        </View>
        <TouchableOpacity
          onPress={() => navigation.navigate('LoginScreen')}
          style={[
            styles.signIn,
            {
              borderColor: '#009387',
              borderWidth: 1,
              marginTop: 15,
            },
          ]}>
          <Text style={[styles.textSign, {color: '#009387'}]}>Login</Text>
        </TouchableOpacity>
      </View>
    </View>
  );
};

export default LoginScreen;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#009387',
  },
  header: {
    flex: 1,
    justifyContent: 'flex-end',
    paddingHorizontal: 10,
    paddingBottom: 40,
  },
  footer: {
    flex: 5,
    backgroundColor: '#fff',
    borderTopLeftRadius: 20,
    borderTopRightRadius: 20,
    paddingHorizontal: 20,
    paddingVertical: 20,
  },
  text_header: {
    color: '#fff',
    fontWeight: 'bold',
    fontSize: 20,
  },
  text_footer: {
    color: '#05375a',
    fontSize: 15,
  },
  action: {
    flexDirection: 'row',
    marginTop: 10,
    borderBottomWidth: 1,
    borderBottomColor: '#f2f2f2',
    paddingBottom: 5,
  },
  actionError: {
    flexDirection: 'row',
    marginTop: 10,
    borderBottomWidth: 1,
    borderBottomColor: '#FF0000',
    paddingBottom: 5,
  },
  textInput: {
    flex: 1,
    marginTop: Platform.OS === 'ios' ? 0 : -12,
    paddingLeft: 10,
    color: '#05375a',
  },
  errorMsg: {
    color: '#FF0000',
    fontSize: 14,
  },
  button: {
    alignItems: 'center',
    marginTop: 20,
  },
  signIn: {
    width: '100%',
    height: 20,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 10,
  },
  textSign: {
    fontSize: 15,
    fontWeight: 'bold',
  },
});
