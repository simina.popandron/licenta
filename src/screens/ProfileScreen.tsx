import React, {useContext, useEffect} from 'react';
import {
  Alert,
  Button,
  SafeAreaView,
  ScrollView,
  StatusBar,
  StyleSheet,
  Text,
  useColorScheme,
  View,
  Image,
  ImageSourcePropType,
} from 'react-native';
// import {Icon} from 'react-native-vector-icons/Ionicons';
import Icon from 'react-native-vector-icons/Ionicons';
import {
  LineChart,
  BarChart,
  PieChart,
  ProgressChart,
  ContributionGraph,
  StackedBarChart,
} from 'react-native-chart-kit';
import {Dimensions} from 'react-native';
import {white} from 'react-native-paper/lib/typescript/styles/colors';
import {UserContext} from '../../App';
import axios from 'axios';
import {useState} from 'react';
import {useIsFocused} from '@react-navigation/native';
import LogoutButton from './LogoutButton';

const API = 'http://10.0.2.2:4020';

const screenWidth = Dimensions.get('window').width;

const chartConfig = {
  backgroundGradientFrom: 'white',
  backgroundGradientFromOpacity: 0,
  backgroundGradientTo: 'white',
  backgroundGradientToOpacity: 0.5,
  color: (opacity = 1) => `rgba(0, 0, 0, ${opacity})`,
  strokeWidth: 4, // optional, default 3
  barPercentage: 0.5,
  useShadowColorFromDataset: false, // optional
};

const dataRides = [
  {
    name: '- linia 46',
    rides: 15,
    color: '#FFAD05',
    legendFontColor: '#7F7F7F',
    legendFontSize: 15,
  },
  {
    name: ' - linia 5',
    rides: 20,
    color: '#40BCD8',
    legendFontColor: '#7F7F7F',
    legendFontSize: 15,
  },
  {
    name: ' - linia 24',
    rides: 4,
    color: '#FF3366',
    legendFontColor: '#7F7F7F',
    legendFontSize: 15,
  },
  {
    name: ' - linia 47',
    rides: 1,
    color: '#1AFFD5',
    legendFontColor: '#7F7F7F',
    legendFontSize: 15,
  },
];

const dataTypes = [
  {
    name: ' - car',
    type: 60,
    color: '#FFAD05',
    legendFontColor: '#7F7F7F',
    legendFontSize: 15,
  },
  {
    name: ' - bus',
    type: 25,
    color: '#40BCD8',
    legendFontColor: '#7F7F7F',
    legendFontSize: 15,
  },
  {
    name: ' - walk',
    type: 15,
    color: '#FF3366',
    legendFontColor: '#7F7F7F',
    legendFontSize: 15,
  },
  {
    name: ' - bike',
    type: 10,
    color: '#1AFFD5',
    legendFontColor: '#7F7F7F',
    legendFontSize: 15,
  },
];

const numberEnding = (place: number) => {
  if (place % 10 === 1) {
    return place + 'st';
  } else if (place % 10 === 2) {
    return place + 'nd';
  } else if (place % 10 === 3) {
    return place + 'rd';
  } else {
    return place + 'th';
  }
};

function ProfileScreen({navigation}: any) {
  const userInfo = useContext(UserContext);

  const [points, setPoints] = useState<number>(0);
  const [place, setPlace] = useState<number>(0);
  const [distance, setDistance] = useState<number>(0);

  const isFocused = useIsFocused();

  useEffect(() => {
    //console.log('USE CONTEXT ', userInfo);
    userTotalPointsCall(userInfo.contextUser.token);
    placeCall(userInfo.contextUser.token);
    distanceCall(userInfo.contextUser.token);
  }, [isFocused]);

  const userTotalPointsCall = async (token: string) => {
    axios
      .get(API + `/user/total-points`, {
        headers: {Authorization: `Bearer ${token}`},
      })
      .then(response => {
        setPoints(response.data);
      })
      .catch(err => console.log(err));
  };

  const placeCall = async (token: string) => {
    axios
      .get(API + `/place`, {
        headers: {Authorization: `Bearer ${token}`},
      })
      .then(response => {
        setPlace(response.data);
      })
      .catch(err => console.log(err));
  };

  const distanceCall = async (token: string) => {
    axios
      .get(API + `/distance`, {
        headers: {Authorization: `Bearer ${token}`},
      })
      .then(response => {
        setDistance(response.data);
      })
      .catch(err => console.log(err));
  };

  return (
    <UserContext.Consumer>
      {({contextUser, setContextUser}) => {
        //console.log('received context user', contextUser);

        //userTotalPointsCall(contextUser.token);
        if (contextUser != null) {
          console.log('username: ', contextUser.username);
        } else {
          console.log('there is no user logged in');
        }

        return (
          // <View style={styles.container}>
          //   <Text>Profile Screen</Text>
          //   <Button
          //     title="Click here"
          //     onPress={() => alert('Button clicked!')}
          //   />
          // </View>
          <View style={styles.container}>
            <ScrollView showsVerticalScrollIndicator={false}>
              {/* <View style={styles.titleBar}>
          <Icon name="arrow-back-outline" size={24} color="#52575D"></Icon>
          <Icon name="grid-outline" size={24} color="#52575D"></Icon>
        </View> */}
              <View style={{alignSelf: 'center'}}>
                <View style={styles.profileImage}>
                  <Image
                    source={
                      {
                        uri: contextUser.photo,
                      } as ImageSourcePropType
                    }
                    style={styles.image}
                    resizeMode="center"></Image>
                </View>
                {/* <View style={styles.dm}>
            <Icon
              name="chatbubbles-outline"
              size={18}
              backgroundColor="#DFD8C8"></Icon>
          </View> */}
                {/* <View style={styles.active}></View>
          <View style={styles.add}>
            <Icon
              name="add"
              size={48}
              color="#DFD8C8"
              style={{marginTop: 6, marginLeft: 2}}></Icon>
          </View> */}
                <View style={styles.infoContainer}>
                  <Text
                    style={[styles.text, {fontWeight: '200', fontSize: 26}]}>
                    {contextUser.username}
                  </Text>
                </View>
              </View>
              <View style={styles.statsContainer}>
                <View style={styles.statsBox}>
                  <Text style={[styles.text, {fontSize: 20}]}>
                    {/* {userTotalPointsCall(contextUser.token)} */}
                    {points}
                  </Text>
                  <Text style={[styles.text, styles.subText]}>Points</Text>
                </View>
                <View style={[styles.statsBox]}>
                  <Text style={[styles.text, {fontSize: 20}]}>
                    {distance * 2.6}
                  </Text>
                  <Text style={[styles.text, styles.subText]}>Steps</Text>
                </View>
                <View style={styles.statsBox}>
                  <Text style={[styles.text, {fontSize: 20}]}>
                    {numberEnding(place)}
                  </Text>
                  <Text style={[styles.text, styles.subText]}>Place</Text>
                </View>
                <View style={styles.statsBox}>
                  <Text style={[styles.text, {fontSize: 20}]}>
                    {distance / 1000}
                  </Text>
                  <Text style={[styles.text, styles.subText]}>km</Text>
                </View>
              </View>
              <View style={{marginTop: 32}}>
                {/* <Text>test1</Text>
          <Text>Public Transportation rides</Text> */}
                <View>
                  <View>
                    {/*It is an Example of Bar Chart*/}
                    <Text
                      style={{
                        textAlign: 'center',
                        fontSize: 18,
                        padding: 16,
                        marginTop: 16,
                      }}>
                      Public Transportation rides
                    </Text>
                    <PieChart
                      data={dataRides}
                      width={screenWidth}
                      height={220}
                      chartConfig={chartConfig}
                      accessor={'rides'}
                      backgroundColor={'transparent'}
                      paddingLeft={'15'}
                      center={[10, 10]}
                      absolute
                    />
                  </View>
                </View>
                <View>
                  <View>
                    {/*It is an Example of Bar Chart*/}
                    <Text
                      style={{
                        textAlign: 'center',
                        fontSize: 18,
                        padding: 16,
                        marginTop: 16,
                      }}>
                      Steps
                    </Text>
                    <BarChart
                      data={{
                        labels: [
                          '26.07',
                          '27.07',
                          '28.07',
                          '29.07',
                          '30.07',
                          '31.07',
                          '01.08',
                        ],
                        datasets: [
                          {
                            data: [1395, 4061, 3359, 8707, 8248, 4340, 4576],
                          },
                        ],
                      }}
                      width={Dimensions.get('window').width - 16}
                      height={220}
                      chartConfig={{
                        backgroundColor: '#FFAD05',
                        backgroundGradientFrom: '#40BCD8',
                        backgroundGradientTo: '#1AFFD5',
                        decimalPlaces: 2,
                        color: (opacity = 1) => `rgba(0, 0, 0, ${opacity})`,
                        style: {
                          borderRadius: 16,
                        },
                      }}
                      style={{marginVertical: 8, borderRadius: 16}}
                    />
                  </View>
                </View>

                <View>
                  <View>
                    <Text
                      style={{
                        textAlign: 'center',
                        fontSize: 18,
                        padding: 16,
                        marginTop: 16,
                      }}>
                      Types of transportation
                    </Text>
                    <PieChart
                      data={dataTypes}
                      width={screenWidth}
                      height={220}
                      chartConfig={chartConfig}
                      accessor={'type'}
                      backgroundColor={'transparent'}
                      paddingLeft={'15'}
                      center={[10, 10]}
                      absolute
                    />
                  </View>
                </View>
                <View>
                  <View>
                    {/*It is an Example of Bar Chart*/}
                    <Text
                      style={{
                        textAlign: 'center',
                        fontSize: 18,
                        padding: 16,
                        marginTop: 16,
                      }}>
                      Points
                    </Text>
                    <BarChart
                      data={{
                        labels: [
                          '26.07',
                          '27.07',
                          '28.07',
                          '29.07',
                          '30.07',
                          '31.07',
                          '01.08',
                        ],
                        datasets: [
                          {
                            data: [20, 45, 28, 80, 99, 43],
                          },
                        ],
                      }}
                      width={Dimensions.get('window').width - 16}
                      height={220}
                      //yAxisLabel={'$'}
                      chartConfig={{
                        backgroundColor: '#FFAD05',
                        backgroundGradientFrom: '#40BCD8',
                        backgroundGradientTo: '#1AFFD5',
                        decimalPlaces: 2,
                        color: (opacity = 1) => `rgba(0, 0, 0, ${opacity})`,
                        style: {
                          borderRadius: 16,
                        },
                      }}
                      style={{marginVertical: 8, borderRadius: 16}}
                    />
                  </View>
                </View>
              </View>
              <LogoutButton />
            </ScrollView>
          </View>
        );
      }}
    </UserContext.Consumer>
  );
}

export default ProfileScreen;

// const styles = StyleSheet.create({
//     container:{
//         flex:1,
//         alignItems:'center',
//         justifyContent: 'center'
//     },
// });

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
  text: {
    fontFamily: 'HelveticaNeue',
    color: '#52575D',
  },
  image: {
    flex: 1,
    width: undefined,
    height: undefined,
  },
  titleBar: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginTop: 24,
    marginHorizontal: 16,
  },
  profileImage: {
    width: 200,
    height: 200,
    borderRadius: 130,
    overflow: 'hidden',
  },
  dm: {
    backgroundColor: '#41444B',
    position: 'absolute',
    top: 20,
    width: 40,
    height: 40,
    borderRadius: 20,
    alignItems: 'center',
    justifyContent: 'center',
  },
  active: {
    backgroundColor: '#34FFB8',
    position: 'absolute',
    bottom: 28,
    left: 10,
    padding: 4,
    height: 20,
    width: 20,
    borderRadius: 10,
  },
  add: {
    backgroundColor: '#41444B',
    position: 'absolute',
    bottom: 0,
    right: 0,
    width: 60,
    height: 60,
    borderRadius: 30,
    alignItems: 'center',
    justifyContent: 'center',
  },
  infoContainer: {
    alignSelf: 'center',
    alignItems: 'center',
    marginTop: 16,
  },
  statsContainer: {
    flexDirection: 'row',
    alignSelf: 'center',
    marginTop: 12,
  },
  statsBox: {
    alignItems: 'center',
    flex: 1,
  },
  subText: {
    fontSize: 12,
    color: '#AEB5BC',
    textTransform: 'uppercase',
    fontWeight: '500',
  },
  containerChart: {
    flex: 1,
    justifyContent: 'center',
    padding: 8,
    paddingTop: 30,
    backgroundColor: 'white',
  },
});
