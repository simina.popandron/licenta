import React, {useEffect} from 'react';
import {createStackNavigator} from '@react-navigation/stack';
import Icon from 'react-native-vector-icons/Ionicons';
import {createMaterialBottomTabNavigator} from '@react-navigation/material-bottom-tabs';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';

import HomeScreen from './HomeScreen';
import DetailsScreen from './DetailsScreen';
import MapScreen from './MapScreen';
import ProfileScreen from './ProfileScreen';
import CurrentLocation from '../CurrentLocation';
import ScanQR from './ScanQR';

//const Stack = createStackNavigator();
const HomeStack = createStackNavigator();
const DetailsStack = createStackNavigator();
const ProfileStack = createStackNavigator();
const MapStack = createStackNavigator();
const Tab = createMaterialBottomTabNavigator();
const ScanStack = createStackNavigator();

const MainTabScreen = props => {
  useEffect(() => {
    console.log('props ', props);
  }, []);

  return (
    <Tab.Navigator
      initialRouteName="Home"
      activeColor="#FF3366"
      barStyle={{backgroundColor: 'tomato'}}>
      <Tab.Screen
        name="Home"
        component={HomeStackScreen}
        options={{
          tabBarLabel: 'Home',
          tabBarColor: '#F4989C',
          tabBarIcon: ({color}) => (
            <MaterialCommunityIcons name="home" color={color} size={26} />
          ),
        }}
      />
      <Tab.Screen
        name="Details"
        component={DetailsStackScreen}
        options={{
          tabBarLabel: 'Top',
          tabBarColor: '#FFAD05',
          tabBarIcon: ({color}) => (
            <MaterialCommunityIcons name="chart-bar" color={color} size={26} />
          ),
        }}
      />
      <Tab.Screen
        name="Profile"
        component={ProfileStackScreen}
        options={{
          tabBarLabel: 'Profile',
          tabBarColor: '#BEA7E5',
          tabBarIcon: ({color}) => (
            <MaterialCommunityIcons name="account" color={color} size={26} />
          ),
        }}
      />
      <Tab.Screen
        name="Map"
        options={{
          tabBarLabel: 'Map',
          tabBarColor: '#40BCD8',
          tabBarIcon: ({color}) => (
            <MaterialCommunityIcons
              name="map-marker-radius"
              color={color}
              size={26}
            />
          ),
        }}>
        {_ => <MapStackScreen helloText={props.helloText} />}
      </Tab.Screen>
      <Tab.Screen
        name="Scan"
        component={ScanStackScreen}
        options={{
          tabBarLabel: 'Scan',
          tabBarColor: '#94FBAB',
          tabBarIcon: ({color}) => (
            <MaterialCommunityIcons
              name="qrcode-scan"
              color={color}
              size={24}
            />
          ),
        }}
      />
    </Tab.Navigator>
  );
};

export default MainTabScreen;

const HomeStackScreen = ({navigation}) => (
  <HomeStack.Navigator
    screenOptions={{
      headerStyle: {
        backgroundColor: '#F4989C',
      },
      headerTintColor: '#fff',
      headerTitleStyle: {
        fontWeight: 'bold',
      },
    }}>
    <HomeStack.Screen
      name="Home"
      component={HomeScreen}
      options={{
        title: 'Home',
        headerLeft: () => (
          <Icon.Button
            name="menu"
            size={25}
            backgroundColor="#F4989C"
            onPress={() => navigation.openDrawer()}></Icon.Button>
        ),
      }}
    />
  </HomeStack.Navigator>
);

const DetailsStackScreen = ({navigation}) => (
  <DetailsStack.Navigator
    screenOptions={{
      headerStyle: {
        backgroundColor: '#FFAD05',
      },
      headerTintColor: '#fff',
      headerTitleStyle: {
        fontWeight: 'bold',
      },
    }}>
    <DetailsStack.Screen
      name="Top"
      component={DetailsScreen}
      options={{
        headerLeft: () => (
          <Icon.Button
            name="menu"
            size={25}
            backgroundColor="#FFAD05"
            onPress={() => {
              navigation.openDrawer();
            }}></Icon.Button>
        ),
      }}
    />
  </DetailsStack.Navigator>
);

const ProfileStackScreen = ({navigation}) => (
  <ProfileStack.Navigator
    screenOptions={{
      headerStyle: {
        backgroundColor: '#BEA7E5',
      },
      headerTintColor: '#fff',
      headerTitleStyle: {
        fontWeight: 'bold',
      },
    }}>
    <ProfileStack.Screen
      name="Profile"
      component={ProfileScreen}
      options={{
        headerLeft: () => (
          <Icon.Button
            name="menu"
            size={25}
            backgroundColor="#BEA7E5"
            onPress={() => navigation.openDrawer()}></Icon.Button>
        ),
      }}
    />
  </ProfileStack.Navigator>
);

const MapStackScreen = props => {
  const {helloText, navigation} = props;

  return (
    <MapStack.Navigator
      screenOptions={{
        headerStyle: {
          backgroundColor: '#40BCD8',
        },
        headerTintColor: '#fff',
        headerTitleStyle: {
          fontWeight: 'bold',
        },
      }}>
      <MapStack.Screen
        name="Map"
        options={{
          headerLeft: () => (
            <Icon.Button
              name="menu"
              size={25}
              backgroundColor="#40BCD8"
              onPress={() => navigation.openDrawer()}></Icon.Button>
          ),
        }}>
        {_ => <CurrentLocation helloText={helloText} />}
      </MapStack.Screen>
    </MapStack.Navigator>
  );
};

const ScanStackScreen = ({navigation}) => (
  <ScanStack.Navigator
    screenOptions={{
      headerStyle: {
        backgroundColor: '#94FBAB',
      },
      headerTintColor: '#fff',
      headerTitleStyle: {
        fontWeight: 'bold',
      },
    }}>
    <ScanStack.Screen
      name="Scan"
      component={ScanQR}
      options={{
        headerLeft: () => (
          <Icon.Button
            name="qrcode-scan"
            size={25}
            backgroundColor="#94FBAB"
            onPress={() => navigation.openDrawer()}></Icon.Button>
        ),
      }}
    />
  </ScanStack.Navigator>
);
