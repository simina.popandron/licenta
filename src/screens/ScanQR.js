'use strict';

import React from 'react';
import {useEffect, useState, useContext} from 'react';
import LinearGradient from 'react-native-linear-gradient';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';

import {
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
  Dimensions,
  Image,
} from 'react-native';

import QRCodeScanner from 'react-native-qrcode-scanner';
import {RNCamera} from 'react-native-camera';
import {RouteContext} from '../../App';
import {useIsFocused} from '@react-navigation/native';

const {height} = Dimensions.get('screen');
const height_logo = height * 0.28;

function ScanQR({navigation}) {
  const [line, setLine] = useState(0);
  const routeInfo = useContext(RouteContext);

  const isFocused = useIsFocused();
  useEffect(() => {
    console.log(routeInfo);
    return function cleanUp() {
      console.log('refreshed');
    };
  }, [isFocused]);

  const onSuccess = e => {
    console.log('QR is scanning');
    console.log('BUS LINE IS: ', e.data);
    setLine(e.data);
    //console.log('line: ', line);
  };

  if (line === 0) {
    return (
      <RouteContext.Consumer>
        {({contextRoute, setContextRoute}) => {
          return (
            <QRCodeScanner
              onRead={onSuccess}
              reactivate={true}
              //cameraProps={{flashMode: RNCamera.Constants.FlashMode.auto}}
              flashMode={RNCamera.Constants.FlashMode.torch}
              topContent={
                <Text style={styles.centerText}>
                  <Text style={styles.textBold}>
                    Scan the QR code you see bus:
                  </Text>{' '}
                </Text>
              }
            />
          );
        }}
      </RouteContext.Consumer>
    );
  } else {
    return (
      <RouteContext.Consumer>
        {({contextRoute, setContextRoute}) => {
          return (
            // <View>
            //   <Text style={styles.title}>Line: {line}</Text>
            //   <TouchableOpacity
            //     //style={styles.size2}
            //     onPress={() => {
            //       setContextRoute({transportLine: line});
            //       console.log('context:', contextRoute);
            //     }}>
            //     <Text style={styles.buttonText}>OK. Got it!</Text>
            //   </TouchableOpacity>
            //   <TouchableOpacity
            //     //style={styles.size2}
            //     onPress={() => {
            //       setContextRoute({transportLine: 0});
            //       console.log('context:', contextRoute);
            //       setLine(0);
            //     }}>
            //     <Text style={styles.buttonText}>Rescan!</Text>
            //   </TouchableOpacity>
            // </View>
            <View style={styles.container}>
              <View style={styles.header}>
                <Image
                  source={require('../../assets/logo.png')}
                  style={styles.logo}
                  resizeMode="stretch"
                />
              </View>
              <View style={styles.footer}>
                <Text style={styles.title}>Line detected: {line}</Text>
                <View style={styles.bigButton}>
                  <TouchableOpacity
                    onPress={() => {
                      setContextRoute({transportLine: line});
                      console.log('context:', contextRoute);
                    }}>
                    <LinearGradient
                      colors={['#94FBAB', '#01ab9d']}
                      style={styles.yesline}>
                      <Text style={styles.textSign}>
                        Yes, this is the right transportation line!
                      </Text>
                      <MaterialIcons
                        name="navigate-next"
                        color="#fff"
                        size={20}
                      />
                    </LinearGradient>
                  </TouchableOpacity>
                </View>
                <View style={styles.button}>
                  <TouchableOpacity
                    onPress={() => {
                      setContextRoute({transportLine: 0});
                      console.log('context:', contextRoute);
                      setLine(0);
                    }}>
                    <LinearGradient
                      colors={['#94FBAB', '#01ab9d']}
                      style={styles.signIn}>
                      <Text style={styles.textSign}>Rescan</Text>
                      <MaterialIcons
                        name="navigate-next"
                        color="#fff"
                        size={20}
                      />
                    </LinearGradient>
                  </TouchableOpacity>
                </View>
              </View>
            </View>
          );
        }}
      </RouteContext.Consumer>
    );
  }
}
export default ScanQR;

const styles = StyleSheet.create({
  centerText: {
    flex: 1,
    fontSize: 18,
    padding: 32,
    color: '#777',
  },
  textBold: {
    fontWeight: '500',
    color: '#000',
  },
  buttonText: {
    fontSize: 21,
    color: 'rgb(0,122,255)',
  },
  buttonTouchable: {
    padding: 16,
  },

  container: {
    flex: 1,
    backgroundColor: '#94FBAB',
  },
  header: {
    flex: 2,
    justifyContent: 'center',
    alignItems: 'center',
  },
  footer: {
    flex: 1,
    backgroundColor: '#fff',
    borderTopLeftRadius: 30,
    borderTopRightRadius: 30,
    paddingVertical: 50,
    paddingHorizontal: 30,
  },
  logo: {
    width: height_logo,
    height: height_logo,
  },
  title: {
    color: '#05375a',
    fontSize: 30,
    fontWeight: 'bold',
  },
  text: {
    color: 'grey',
    marginTop: 5,
  },
  button: {
    alignItems: 'flex-end',
    marginTop: 30,
  },
  bigButton: {
    alignItems: 'flex-end',
    marginTop: 30,
  },
  signIn: {
    width: 150,
    height: 50,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 50,
    flexDirection: 'row',
  },
  yesline: {
    width: 300,
    height: 50,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 50,
    flexDirection: 'row',
  },
  textSign: {
    color: 'white',
    fontWeight: 'bold',
  },
});
