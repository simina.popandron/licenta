import React from 'react';
 import {
     Alert,
   Button,
   SafeAreaView,
   ScrollView,
   StatusBar,
   StyleSheet,
   Text,
   useColorScheme,
   View,
 } from 'react-native';

 function ProfileScreen() {
    return (
      <View style={styles.container}>
        <Text>Profile Screen</Text>
        <Button
          title="Click here"
          onPress={() => alert('Button clicked!')}
        />
      </View>
    );
  }

  export default ProfileScreen;

  const styles = StyleSheet.create({
      container:{
          flex:1,
          alignItems:'center',
          justifyContent: 'center'
      },
  });