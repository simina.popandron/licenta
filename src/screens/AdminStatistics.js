import React from 'react';
import {
  Alert,
  Button,
  SafeAreaView,
  ScrollView,
  StatusBar,
  StyleSheet,
  Text,
  useColorScheme,
  View,
  Image,
} from 'react-native';
// import {Icon} from 'react-native-vector-icons/Ionicons';
import Icon from 'react-native-vector-icons/Ionicons';
import {
  LineChart,
  BarChart,
  PieChart,
  ProgressChart,
  ContributionGraph,
  StackedBarChart,
} from 'react-native-chart-kit';
import {Dimensions} from 'react-native';
import {white} from 'react-native-paper/lib/typescript/styles/colors';
import {UserContext} from '../../App';

const screenWidth = Dimensions.get('window').width;

const chartConfig = {
  backgroundGradientFrom: 'white',
  backgroundGradientFromOpacity: 0,
  backgroundGradientTo: 'white',
  backgroundGradientToOpacity: 0.5,
  color: (opacity = 1) => `rgba(0, 0, 0, ${opacity})`,
  strokeWidth: 4, // optional, default 3
  barPercentage: 0.5,
  useShadowColorFromDataset: false, // optional
};

const dataRides = [
  {
    name: ' - 46B',
    rides: 67,
    color: '#FFAD05',
    legendFontColor: '#7F7F7F',
    legendFontSize: 15,
  },
  {
    name: ' - 5',
    rides: 235,
    color: '#40BCD8',
    legendFontColor: '#7F7F7F',
    legendFontSize: 15,
  },
  {
    name: ' - 24B',
    rides: 45,
    color: '#FF3366',
    legendFontColor: '#7F7F7F',
    legendFontSize: 15,
  },
  {
    name: ' - 47',
    rides: 66,
    color: '#1AFFD5',
    legendFontColor: '#7F7F7F',
    legendFontSize: 15,
  },
  {
    name: ' - 30',
    rides: 573,
    color: '#EBC61E',
    legendFontColor: '#7F7F7F',
    legendFontSize: 15,
  },
  {
    name: ' - 25',
    rides: 420,
    color: '#04E762',
    legendFontColor: '#7F7F7F',
    legendFontSize: 15,
  },
  {
    name: ' - 42',
    rides: 12,
    color: '#DE172E',
    legendFontColor: '#7F7F7F',
    legendFontSize: 15,
  },
];

const dataTypes = [
  {
    name: ' - car',
    type: 972,
    color: '#FFAD05',
    legendFontColor: '#7F7F7F',
    legendFontSize: 15,
  },
  {
    name: ' - bus',
    type: 1418,
    color: '#40BCD8',
    legendFontColor: '#7F7F7F',
    legendFontSize: 15,
  },
  {
    name: ' - walk',
    type: 2930,
    color: '#FF3366',
    legendFontColor: '#7F7F7F',
    legendFontSize: 15,
  },
  {
    name: ' - bike',
    type: 362,
    color: '#1AFFD5',
    legendFontColor: '#7F7F7F',
    legendFontSize: 15,
  },
];
function AdminStatistics() {
  return (
    <UserContext.Consumer>
      {({contextUser, setContextUser}) => {
        console.log('received context user', contextUser);
        if (contextUser != null) {
          console.log('username: ', contextUser.username);
        } else {
          console.log('there is no user logged in');
        }

        return (
          <View style={styles.container}>
            <ScrollView showsVerticalScrollIndicator={false}>
              <View style={{alignSelf: 'center'}}>
                <View style={styles.infoContainer}>
                  <Text
                    style={[styles.text, {fontWeight: '200', fontSize: 26}]}>
                    Overall User Statistics
                  </Text>
                </View>
              </View>
              <View>
                <View>
                  <View>
                    <Text
                      style={{
                        textAlign: 'center',
                        fontSize: 18,
                        padding: 16,
                        marginTop: 16,
                      }}>
                      Public Transportation rides
                    </Text>
                    <PieChart
                      data={dataRides}
                      width={screenWidth}
                      height={220}
                      chartConfig={chartConfig}
                      accessor={'rides'}
                      backgroundColor={'transparent'}
                      paddingLeft={'15'}
                      center={[10, 10]}
                      absolute
                    />
                  </View>
                </View>
                <View>
                  <View>
                    <Text
                      style={{
                        textAlign: 'center',
                        fontSize: 18,
                        padding: 16,
                        marginTop: 16,
                      }}>
                      Average number of steps per user
                    </Text>
                    <BarChart
                      data={{
                        labels: [
                          '26.07',
                          '27.07',
                          '28.07',
                          '29.07',
                          '30.07',
                          '31.07',
                          '01.08',
                        ],
                        datasets: [
                          {
                            data: [3457, 2346, 5382, 4601, 2247, 9547, 8750],
                          },
                        ],
                      }}
                      width={Dimensions.get('window').width - 16}
                      height={250}
                      chartConfig={{
                        backgroundColor: '#FFAD05',
                        backgroundGradientFrom: '#40BCD8',
                        backgroundGradientTo: '#1AFFD5',
                        decimalPlaces: 2,
                        color: (opacity = 1) => `rgba(0, 0, 0, ${opacity})`,
                        style: {
                          borderRadius: 16,
                        },
                      }}
                      style={{marginVertical: 8, borderRadius: 16}}
                    />
                  </View>
                </View>

                <View>
                  <View>
                    <Text
                      style={{
                        textAlign: 'center',
                        fontSize: 18,
                        padding: 16,
                        marginTop: 16,
                      }}>
                      Types of transportation
                    </Text>
                    <PieChart
                      data={dataTypes}
                      width={screenWidth}
                      height={220}
                      chartConfig={chartConfig}
                      accessor={'type'}
                      backgroundColor={'transparent'}
                      paddingLeft={'15'}
                      center={[10, 10]}
                      absolute
                    />
                  </View>
                </View>
                <View>
                  <View>
                    <Text
                      style={{
                        textAlign: 'center',
                        fontSize: 18,
                        padding: 16,
                        marginTop: 16,
                      }}>
                      Average number of points per user
                    </Text>
                    <BarChart
                      data={{
                        labels: [
                          '26.07',
                          '27.07',
                          '28.07',
                          '29.07',
                          '30.07',
                          '31.07',
                          '01.08',
                        ],
                        datasets: [
                          {
                            data: [12, 25, 22, 34, 56, 47],
                          },
                        ],
                      }}
                      width={Dimensions.get('window').width - 16}
                      height={250}
                      chartConfig={{
                        backgroundColor: '#FFAD05',
                        backgroundGradientFrom: '#40BCD8',
                        backgroundGradientTo: '#1AFFD5',
                        decimalPlaces: 2,
                        color: (opacity = 1) => `rgba(0, 0, 0, ${opacity})`,
                        style: {
                          borderRadius: 16,
                        },
                      }}
                      style={{marginVertical: 8, borderRadius: 16}}
                    />
                  </View>
                </View>
              </View>
            </ScrollView>
          </View>
        );
      }}
    </UserContext.Consumer>
  );
}

export default AdminStatistics;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
  text: {
    fontFamily: 'HelveticaNeue',
    color: '#52575D',
  },
  image: {
    flex: 1,
    width: undefined,
    height: undefined,
  },
  titleBar: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginTop: 24,
    marginHorizontal: 16,
  },
  profileImage: {
    width: 200,
    height: 200,
    borderRadius: 130,
    overflow: 'hidden',
  },
  dm: {
    backgroundColor: '#41444B',
    position: 'absolute',
    top: 20,
    width: 40,
    height: 40,
    borderRadius: 20,
    alignItems: 'center',
    justifyContent: 'center',
  },
  active: {
    backgroundColor: '#34FFB8',
    position: 'absolute',
    bottom: 28,
    left: 10,
    padding: 4,
    height: 20,
    width: 20,
    borderRadius: 10,
  },
  add: {
    backgroundColor: '#41444B',
    position: 'absolute',
    bottom: 0,
    right: 0,
    width: 60,
    height: 60,
    borderRadius: 30,
    alignItems: 'center',
    justifyContent: 'center',
  },
  infoContainer: {
    alignSelf: 'center',
    alignItems: 'center',
    marginTop: 16,
  },
  statsContainer: {
    flexDirection: 'row',
    alignSelf: 'center',
    marginTop: 12,
  },
  statsBox: {
    alignItems: 'center',
    flex: 1,
  },
  subText: {
    fontSize: 12,
    color: '#AEB5BC',
    textTransform: 'uppercase',
    fontWeight: '500',
  },
  containerChart: {
    flex: 1,
    justifyContent: 'center',
    padding: 8,
    paddingTop: 30,
    backgroundColor: 'white',
  },
});
