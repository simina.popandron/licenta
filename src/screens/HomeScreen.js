//import React from 'react';

import React, {Component} from 'react';
import {Provider, Toast, Button} from '@ant-design/react-native';
import {DestinationContext} from '../App';
('use strict');

import QRCodeScanner from 'react-native-qrcode-scanner';
import {RNCamera} from 'react-native-camera';

import {
  SafeAreaView,
  ScrollView,
  StatusBar,
  StyleSheet,
  Text,
  useColorScheme,
  View,
  List,
  ListItem,
  AppRegistry,
  TouchableOpacity,
  Linking,
} from 'react-native';
import {UserContext} from '../../App';
//import {List} from 'react-native-paper';
//import {ListItem} from 'react-native-elements/dist/list/ListItem';
import LogoutButton from './LogoutButton';

function HomeScreen({navigation}) {
  const onSuccess = e => {
    Linking.openURL(e.data).catch(err =>
      console.error('An error occured', err),
    );
  };
  return (
    <UserContext.Consumer>
      {({contextUser, setContextUser}) => (
        <ScrollView>
          <View style={styles.container}>
            <Provider>
              <Text style={styles.flattenStyle}>Welcome!</Text>
              <Text
                style={{
                  textAlign: 'center',
                  fontSize: 18,
                  fontFamily: 'HelveticaNeue',
                  color: '#52575D',
                }}>
                Here is a guide on how the app works:
              </Text>
              <View style={styles.nestedViewStyle}>
                <Text>{'     '}</Text>
              </View>
              <Text style={styles.simpleText}>
                At the bottom of the page you can see 5 tabs :
              </Text>
              <Text style={styles.simpleText}>{'\u2022'} Home</Text>
              <Text style={styles.simpleText}>{'\u2022'} Top</Text>
              <Text style={styles.simpleText}>{'\u2022'} Profile</Text>
              <Text style={styles.simpleText}>{'\u2022'} Map</Text>
              <Text style={styles.simpleText}>{'\u2022'} Scanner</Text>
              <View style={styles.nestedViewStyle}>
                <Text>{'     '}</Text>
              </View>
              <Text style={styles.simpleText}>
                In the current "Home" tab you can find information about how to
                use the app.
              </Text>
              <Text style={styles.simpleText}>
                In the "Top" tab you can find a top with all of our users
                according to what overall score they have.
              </Text>
              <Text style={styles.simpleText}>
                In the "Profile" tab you can find various statistics about your
                transportation usage.
              </Text>
              <View style={styles.nestedViewStyle}>
                <Text>{'     '}</Text>
              </View>
              <Text style={styles.simpleText}>
                And the fun part beging in the "Map" Tab. There you can start a
                journey by pressing the "START" button and choose a destination.
                After this you will see 3 lines on the screen:
              </Text>
              <Text style={styles.simpleText}>
                {'\u2022'} BLACK - This line is the route if you want to use the
                car
              </Text>
              <Text style={styles.simpleText}>
                {'\u2022'} RED - This line shows the route if you want to choose
                some kind of public transportation
              </Text>
              <Text style={styles.simpleText}>
                {'\u2022'} GREEN - This line is for the walking route
              </Text>
              <Text style={styles.simpleText}>
                The app will track your route and it will appear with PURPLE on
                the screen. When you get to your destination please press the
                "STOP" button and the app will stop tranking you.
              </Text>
              <View style={styles.nestedViewStyle}>
                <Text>{'     '}</Text>
              </View>
              <Text style={styles.simpleText}>
                Each type of journey gets you a score, and it depends on the
                transportation mode:
              </Text>
              <Text style={styles.simpleText}>{'\u2022'} Car - 1 point </Text>
              <Text style={styles.simpleText}>
                {'\u2022'} Public Transportation - 5 points{' '}
              </Text>
              <Text style={styles.simpleText}>{'\u2022'} Bike - 7 points </Text>
              <Text style={styles.simpleText}>
                {'\u2022'} Walking - 10 Points
              </Text>
              <View style={styles.nestedViewStyle}>
                <Text>{'     '}</Text>
              </View>
              <Text style={styles.simpleText}>
                In the "Scanner" tab you can scan the QR codes you find on
                busses in order to record the line you are using. After using
                the scanner once you will see the line. You can also rescan if
                the outcome isnțt right.
              </Text>
              <View style={styles.nestedViewStyle}>
                <Text>{'     '}</Text>
              </View>
              {/* <LogoutButton /> */}
              <Text style={styles.flattenStyle}>Have fun!</Text>
            </Provider>
          </View>
        </ScrollView>
      )}
    </UserContext.Consumer>
  );
}

export default HomeScreen;

AppRegistry.registerComponent('HomeScreen', () => HomeScreen);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#FDE2E4',
  },
  baseText: {
    fontFamily: 'Cochin',
  },
  titleText: {
    fontSize: 20,
    fontWeight: 'bold',
  },
  flattenStyle: {
    color: '#000',
    fontSize: 14,
    fontWeight: 'bold',
    color: '#FF3366',
    fontSize: 30,
    marginBottom: 36,
    textAlign: 'center',
  },
  nestedViewStyle: {
    width: 10,
    marginVertical: 1,
  },
  simpleText: {
    fontFamily: 'HelveticaNeue',
    color: '#495057',
  },
  centerText: {
    flex: 1,
    fontSize: 18,
    padding: 32,
    color: '#777',
  },
  textBold: {
    fontWeight: '500',
    color: '#000',
  },
  buttonText: {
    fontSize: 21,
    color: 'rgb(0,122,255)',
  },
  buttonTouchable: {
    padding: 16,
  },
});
