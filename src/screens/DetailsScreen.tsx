import {useIsFocused} from '@react-navigation/native';
import axios from 'axios';
import React, {useContext, useEffect, useState} from 'react';
import {ScrollView, Text, View} from 'react-native';
import {Avatar, ListItem} from 'react-native-elements';
import {UserContext} from '../../App';

const API = 'http://10.0.2.2:4020';

interface IResponseTop {
  photo: string;
  totalPoints: number;
  username: string;
}

interface IUsersList {
  name: string;
  avatar_url: string;
  subtitle: string;
}

const DetailsScreen = ({navigation}: any) => {
  const userInfo = useContext(UserContext);

  const [usersList, setusersList] = useState<IUsersList[]>();
  const isFocused = useIsFocused();
  useEffect(() => {
    topCall(userInfo.contextUser.token);
    //console.log('top users list:', usersList);
    return function cleanUp() {
      console.log('exited');
    };
  }, [isFocused]);

  const topCall = async (token: string) => {
    axios
      .get(API + `/top`, {
        headers: {Authorization: `Bearer ${token}`},
      })
      .then(response => {
        const mappedUserList = response.data.map((user: IResponseTop) => ({
          name: user.username,
          avatar_url: user.photo,
          subtitle: `${user.totalPoints} Points`,
        }));
        setusersList(mappedUserList);
      })
      .catch(err => console.log(err));
  };

  return (
    <ScrollView>
      <View>
        {!usersList ? (
          <Text>loading...</Text>
        ) : (
          usersList.map((l, i) => (
            <ListItem key={i} bottomDivider>
              <Avatar source={{uri: l.avatar_url}} />
              <ListItem.Content>
                <ListItem.Title>{l.name}</ListItem.Title>
                <ListItem.Subtitle>{l.subtitle}</ListItem.Subtitle>
              </ListItem.Content>
            </ListItem>
          ))
        )}
      </View>
    </ScrollView>
  );
};

export default DetailsScreen;
