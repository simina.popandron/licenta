import React, {useEffect} from 'react';
import {createStackNavigator} from '@react-navigation/stack';
import Icon from 'react-native-vector-icons/Ionicons';
import {createMaterialBottomTabNavigator} from '@react-navigation/material-bottom-tabs';
import MaterialCommunityIcons from 'react-native-vector-icons/MaterialCommunityIcons';

import HomeScreenAdmin from './HomeScreenAdmin';
import DetailsScreen from './DetailsScreen';
import MapScreen from './MapScreen';
import ProfileScreen from './ProfileScreen';
import CurrentLocation from '../CurrentLocation';
import AdminStatistics from './AdminStatistics';
import {View, Text} from 'react-native';
import ManageUsers from '../ManageUsers';
import ManageTransport from '../ManageTransport';

//const Stack = createStackNavigator();
const HomeStack = createStackNavigator();
const DetailsStack = createStackNavigator();
const ProfileStack = createStackNavigator();
const MapStack = createStackNavigator();
const Tab = createMaterialBottomTabNavigator();
const AdminStatisticsStack = createStackNavigator();
const ManageUsersStack = createStackNavigator();
const ManageTransportStack = createStackNavigator();

const MainTabScreen = props => {
  useEffect(() => {
    console.log('props ', props);
  }, []);

  return (
    <Tab.Navigator
      initialRouteName="Home"
      activeColor="black"
      barStyle={{backgroundColor: '#DE6B48'}}>
      <Tab.Screen
        name="Home"
        component={HomeStackScreen}
        options={{
          tabBarLabel: 'Home',
          tabBarColor: 'red',
          tabBarIcon: ({color}) => (
            <MaterialCommunityIcons name="home" color={color} size={26} />
          ),
        }}
      />
      <Tab.Screen
        name="Top"
        component={DetailsStackScreen}
        options={{
          tabBarLabel: 'Top',
          tabBarColor: 'red',
          tabBarIcon: ({color}) => (
            <MaterialCommunityIcons name="chart-bar" color={color} size={26} />
          ),
        }}
      />
      <Tab.Screen
        name="Admin Statistics"
        component={AdminStatisticsStackScreen}
        options={{
          tabBarLabel: 'Admin Statistics',
          tabBarColor: 'red',
          tabBarIcon: ({color}) => (
            <MaterialCommunityIcons name="account" color={color} size={26} />
          ),
        }}
      />
      <Tab.Screen
        name="Manage Users"
        component={ManageUsersStackScreen}
        options={{
          tabBarLabel: 'Manage Users',
          tabBarColor: 'red',
          tabBarIcon: ({color}) => (
            <MaterialCommunityIcons
              name="account-group"
              color={color}
              size={26}
            />
          ),
        }}
      />
      <Tab.Screen
        name="Manage Transport"
        component={ManageTransportStackScreen}
        options={{
          tabBarLabel: 'Manage Transport',
          tabBarColor: 'red',
          tabBarIcon: ({color}) => (
            <MaterialCommunityIcons
              name="bus-multiple"
              color={color}
              size={26}
            />
          ),
        }}
      />
    </Tab.Navigator>
  );
};

export default MainTabScreen;

const HomeStackScreen = ({navigation}) => (
  <HomeStack.Navigator
    screenOptions={{
      headerStyle: {
        backgroundColor: 'red',
      },
      headerTintColor: '#fff',
      headerTitleStyle: {
        fontWeight: 'bold',
      },
    }}>
    <HomeStack.Screen
      name="Home"
      component={HomeScreenAdmin}
      options={{
        title: 'Home',
        headerLeft: () => (
          <Icon.Button
            name="menu"
            size={25}
            backgroundColor="red"
            onPress={() => navigation.openDrawer()}></Icon.Button>
        ),
      }}
    />
  </HomeStack.Navigator>
);

const DetailsStackScreen = ({navigation}) => (
  <DetailsStack.Navigator
    screenOptions={{
      headerStyle: {
        backgroundColor: 'red',
      },
      headerTintColor: '#fff',
      headerTitleStyle: {
        fontWeight: 'bold',
      },
    }}>
    <DetailsStack.Screen
      name="Top"
      component={DetailsScreen}
      options={{
        headerLeft: () => (
          <Icon.Button
            name="menu"
            size={25}
            backgroundColor="red"
            onPress={() => navigation.openDrawer()}></Icon.Button>
        ),
      }}
    />
  </DetailsStack.Navigator>
);

const AdminStatisticsStackScreen = ({navigation}) => (
  <AdminStatisticsStack.Navigator
    screenOptions={{
      headerStyle: {
        backgroundColor: 'red',
      },
      headerTintColor: '#fff',
      headerTitleStyle: {
        fontWeight: 'bold',
      },
    }}>
    <AdminStatisticsStack.Screen
      name="Admin Statistics"
      component={AdminStatistics}
      options={{
        headerLeft: () => (
          <Icon.Button
            name="menu"
            size={25}
            backgroundColor="red"
            onPress={() => navigation.openDrawer()}></Icon.Button>
        ),
      }}
    />
  </AdminStatisticsStack.Navigator>
);

const ManageUsersStackScreen = ({navigation}) => (
  <ManageUsersStack.Navigator
    screenOptions={{
      headerStyle: {
        backgroundColor: 'red',
      },
      headerTintColor: '#fff',
      headerTitleStyle: {
        fontWeight: 'bold',
      },
    }}>
    <ManageUsersStack.Screen
      name="Manage Users"
      component={ManageUsers}
      options={{
        headerLeft: () => (
          <Icon.Button
            name="account-group"
            size={25}
            backgroundColor="red"
            onPress={() => navigation.openDrawer()}></Icon.Button>
        ),
      }}
    />
  </ManageUsersStack.Navigator>
);

const ManageTransportStackScreen = ({navigation}) => (
  <ManageTransportStack.Navigator
    screenOptions={{
      headerStyle: {
        backgroundColor: 'red',
      },
      headerTintColor: '#fff',
      headerTitleStyle: {
        fontWeight: 'bold',
      },
    }}>
    <ManageTransportStack.Screen
      name="Manage Transport"
      component={ManageTransport}
      options={{
        headerLeft: () => (
          <Icon.Button
            name="bus-multiple"
            size={25}
            backgroundColor="red"
            onPress={() => navigation.openDrawer()}></Icon.Button>
        ),
      }}
    />
  </ManageTransportStack.Navigator>
);
