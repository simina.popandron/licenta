// import React from 'react';
//  import {
//      Alert,
//    Button,
//    SafeAreaView,
//    ScrollView,
//    StatusBar,
//    StyleSheet,
//    Text,
//    useColorScheme,
//    View,
//    Platform
//  } from 'react-native';
//  import MapView, { PROVIDER_GOOGLE } from 'react-native-maps';

//  function MapScreen() {
//     return (
//       <View style={styles.container}>
//         <Text>Map Screen</Text>
//         <Text>test</Text>
//         <Text>test</Text>
//         <Button
//           title="Click here"
//           onPress={() => alert('Button clicked!')}
//         />
//          {/* <MapView
//             style={{flex: 1}}
//             region={{          latitude: 42.882004,          longitude: 74.582748,          latitudeDelta: 0.0922,          longitudeDelta: 0.0421        }}
//             showsUserLocation={true}
//           /> */}

//         </View>
//     );
//   }

//   export default MapScreen;

//   const styles = StyleSheet.create({
//       container:{
//           flex:1,
//           backgroundColor: '#fff',
//           alignItems:'center',
//           justifyContent: 'center'
//       },
//   });

import React, {Component} from 'react';
import {Platform, StyleSheet, Text, View} from 'react-native';
import MapView, {PROVIDER_GOOGLE} from 'react-native-maps';
export default class MapScreen extends React.Component {
  render() {
    return (
      <View>
        <MapView
          style={{flex: 1}}
          provider={PROVIDER_GOOGLE}
          showsUserLocation
          initialRegion={{
            latitude: 37.78825,
            longitude: -122.4324,
            latitudeDelta: 0.0922,
            longitudeDelta: 0.0421,
          }}
        />
      </View>
    );
  }
}
