import React, {Component} from 'react';
import {Provider, Toast, Button} from '@ant-design/react-native';
import {DestinationContext} from '../App';
('use strict');

import QRCodeScanner from 'react-native-qrcode-scanner';
import {RNCamera} from 'react-native-camera';

import {
  SafeAreaView,
  ScrollView,
  StatusBar,
  StyleSheet,
  Text,
  useColorScheme,
  View,
  List,
  ListItem,
  AppRegistry,
  TouchableOpacity,
  Linking,
} from 'react-native';
import {UserContext} from '../../App';

import LogoutButton from './LogoutButton';

function HomeScreen({navigation}) {
  const onSuccess = e => {
    Linking.openURL(e.data).catch(err =>
      console.error('An error occured', err),
    );
  };
  return (
    <UserContext.Consumer>
      {({contextUser, setContextUser}) => (
        <ScrollView>
          <View style={styles.container}>
            <Provider>
              <Text style={styles.flattenStyle}>Welcome!</Text>
              <Text
                style={{
                  textAlign: 'center',
                  fontSize: 18,
                  fontFamily: 'HelveticaNeue',
                  color: '#52575D',
                }}>
                Here is a guide on how the app works:
              </Text>
              <View style={styles.nestedViewStyle}>
                <Text>{'     '}</Text>
              </View>
              <Text style={styles.simpleText}>
                At the bottom of the page you can see 5 tabs :
              </Text>
              <Text style={styles.simpleText}>{'\u2022'} Home</Text>
              <Text style={styles.simpleText}>{'\u2022'} Top</Text>
              <Text style={styles.simpleText}>{'\u2022'} Statistics</Text>
              <Text style={styles.simpleText}>{'\u2022'} Manage users</Text>
              <Text style={styles.simpleText}>
                {'\u2022'} Manage public transport lines
              </Text>
              <View style={styles.nestedViewStyle}>
                <Text>{'     '}</Text>
              </View>
              <Text style={styles.simpleText}>
                In the current "Home" tab you can find information about how to
                use the app.
              </Text>
              <Text style={styles.simpleText}>
                In the "Top" tab you can find a top with all of our users
                according to what overall score they have.
              </Text>
              <Text style={styles.simpleText}>
                In the "Statistics" tab you can find various statistics about
                the overall transportation usage.
              </Text>
              <Text style={styles.simpleText}>
                In the "Manage users" tab you can view all existing users and
                delete accounts.
              </Text>
              <Text style={styles.simpleText}>
                In the "Manage public transport lines" tab you can view all
                existing lines, add new ones or delete from the existing ones.
              </Text>
              <View style={styles.nestedViewStyle}>
                <Text>{'     '}</Text>
              </View>
              <Text style={styles.flattenStyle}>Have fun!</Text>

              <LogoutButton />
            </Provider>
          </View>
        </ScrollView>
      )}
    </UserContext.Consumer>
  );
}

export default HomeScreen;

AppRegistry.registerComponent('HomeScreen', () => HomeScreen);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#FFF9EB',
  },
  baseText: {
    fontFamily: 'Cochin',
  },
  titleText: {
    fontSize: 20,
    fontWeight: 'bold',
  },
  flattenStyle: {
    color: '#000',
    fontSize: 14,
    fontWeight: 'bold',
    color: '#F53900',
    fontSize: 30,
    marginBottom: 36,
    textAlign: 'center',
  },
  nestedViewStyle: {
    width: 10,
    marginVertical: 1,
  },
  simpleText: {
    fontFamily: 'HelveticaNeue',
    color: '#495057',
  },
  centerText: {
    flex: 1,
    fontSize: 18,
    padding: 32,
    color: '#777',
  },
  textBold: {
    fontWeight: '500',
    color: '#000',
  },
  buttonText: {
    fontSize: 21,
    color: 'rgb(0,122,255)',
  },
  buttonTouchable: {
    padding: 16,
  },
});
