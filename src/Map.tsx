import React from 'react';
import {StyleSheet, Text, View} from 'react-native';
import MapView, {Callout, Marker, PROVIDER_GOOGLE} from 'react-native-maps';

const styles = StyleSheet.create({
  container: {
    ...StyleSheet.absoluteFillObject,
    height: '100%',
    width: '100%',
    justifyContent: 'flex-end',
    alignItems: 'center',
  },
  map: {
    ...StyleSheet.absoluteFillObject,
  },
});

const Map = () => {
  return (
    <View style={styles.container}>
      <MapView
        provider={PROVIDER_GOOGLE} // remove if not using Google Maps
        style={styles.map}
        region={{
          latitude: 46.77762473213953,
          longitude: 23.593413315518053,
          latitudeDelta: 0.015,
          longitudeDelta: 0.0121,
        }}>
        <Marker
          coordinate={{
            longitude: 23.593413315518053,
            latitude: 46.77762473213953,
          }}>
          <Callout>
            <View>
              <Text>Party la mine acasa</Text>
              <Text>Playing now: </Text>
              <Text>Florin Salam - Cum sa nu beau 2 zile</Text>
              <Text>Invitat special: Veliche</Text>
            </View>
          </Callout>
        </Marker>
      </MapView>
    </View>
  );
};

export default Map;
