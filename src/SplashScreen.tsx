import alert from '@ant-design/react-native/lib/modal/alert';
import {NavigationContainer} from '@react-navigation/native';
import React from 'react';

import {
  View,
  Text,
  StyleSheet,
  Dimensions,
  Button,
  Image,
  TouchableOpacity,
  Alert,
} from 'react-native';
import LinearGradient from 'react-native-linear-gradient';
import MaterialIcons from 'react-native-vector-icons/MaterialIcons';
//import * as Animatable from 'react-native-animatable';
import {UserContext} from '../App';

const SplashScreen = ({navigation}: any) => {
  return (
    <UserContext.Consumer>
      {({contextUser, setContextUser}) => (
        <View style={styles.container}>
          <View style={styles.header}>
            <Image
              //animation="bounceIn"
              //duration={1500}
              source={require('../assets/logo.png')}
              style={styles.logo}
              resizeMode="stretch"
            />
          </View>
          <View style={styles.footer}>
            <Text style={styles.title}>Take care of how you travel!</Text>
            <Text style={styles.text}>Sign in with account</Text>
            <View style={styles.button}>
              <TouchableOpacity
                onPress={() => {
                  navigation.navigate('LoginScreen');
                  //   console.log('STARE INITIALA DIN CONTEXT:');
                  //   console.log('username:' + contextUser.username);
                  //   console.log('id:' + contextUser.id);
                  //   console.log('role:' + contextUser.role);
                }}>
                <LinearGradient
                  colors={['#08d4c4', '#01ab9d']}
                  style={styles.signIn}>
                  <Text style={styles.textSign}>Get Started</Text>
                  <MaterialIcons name="navigate-next" color="#fff" size={20} />
                </LinearGradient>
              </TouchableOpacity>
            </View>
          </View>
        </View>
      )}
    </UserContext.Consumer>
  );
};

export default SplashScreen;

const {height} = Dimensions.get('screen');
const height_logo = height * 0.28;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#009387',
  },
  header: {
    flex: 2,
    justifyContent: 'center',
    alignItems: 'center',
  },
  footer: {
    flex: 1,
    backgroundColor: '#fff',
    borderTopLeftRadius: 30,
    borderTopRightRadius: 30,
    paddingVertical: 50,
    paddingHorizontal: 30,
  },
  logo: {
    width: height_logo,
    height: height_logo,
  },
  title: {
    color: '#05375a',
    fontSize: 30,
    fontWeight: 'bold',
  },
  text: {
    color: 'grey',
    marginTop: 5,
  },
  button: {
    alignItems: 'flex-end',
    marginTop: 30,
  },
  signIn: {
    width: 150,
    height: 40,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 50,
    flexDirection: 'row',
  },
  textSign: {
    color: 'white',
    fontWeight: 'bold',
  },
});
